// Copyright (C) 2020
// Todos los derechos reservados

package labcodingstandards;

import java.util.Scanner;


/**
 * @author Grace Hilzinger
 *
 */


public class Calculator {
//CHECKSTYLE:OFF
/**
* Opens a menu of arithmetic operations.
*/
	public static void main(String[] args) {

//CHECKSTYLE:ON
		Scanner reader = new Scanner(System.in);

        System.out.print("1. +\n2. -\n3. *\n4. /\nEnter an operator: ");

        char userOperator = reader.nextLine().charAt(0);
        double userFirst;
        double userSecond;
        String input;

        while (true) {
        	System.out.print("Enter first number: ");
        	input = reader.nextLine();

            try {
            	userFirst = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }

        while (true) {
        	System.out.print("Enter second number: ");
        	input = reader.nextLine();

            try {
            	userSecond = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Not valid!");
            }
        }

        Calculator cal = new Calculator();
        String result = cal.operation(userFirst, userSecond, userOperator);

        System.out.printf(result);
		reader.close();
	}

	/**
	 * Opens a menu of arithmetic operations.
	 * @param operator is the option chosen
	 * @param first is the first operand
	 * @param second is the second operand
	 * @return a String of a message with the result
	 */
	private String operation(double first, double second, char operator) {
		double result = 0;
		switch (operator) {
            case '1':
                result = first + second;
                break;
            case '2':
                result = first - second;
                break;
            case '3':
                result = first * second;
                break;
            case '4':
                result = first / second;
                break;
            default:
            	return "Error! operator is not correct";
        }
		return "The result is: " + result;
	}
}
